provider "google"{
  credentials = file("baybars-301818-b299ae551dec.json")
    }
 module "kubernetes-engine" {
  name  = "hbbackend-dotnet-cluster"
  source  = "terraform-google-modules/kubernetes-engine/google"
  project_id   = "baybars-301818" 
  region = "europe-west1"
  zones= ["europe-west1-b","europe-west1-c","europe-west1-d"]
  network= "default"
  subnetwork= "default"
  ip_range_pods= "us-central1-01-gke-01-pods"
  ip_range_services= "us-central1-01-gke-01-services"
  horizontal_pod_autoscaling = true
  network_policy= true
  default_max_pods_per_node = "10" 
  http_load_balancing = true
  cluster_resource_labels ={"key": "value"} #Aynı key value sistemiyle eşleşen deploylar podlarla örtüşcek	
   cluster_autoscaling = { 
      enabled = true
      min_cpu_cores = 1
      max_cpu_cores = 2
      min_memory_gb = 1
      ax_memory_gb = 7
    }
  enable_vertical_pod_autoscaling = true
  identity_namespace = "baybars-301818.svc.id.goog" 
  enable_shielded_nodes = true
  kubernetes_version = "1.17.15-gke.800"
  node_pools = [
    {
      name = "pool-1"
      initial_node_count = "0"
      default_max_pods_per_node = "25" 
      disk_type = "pd-ssd" 
      disk_size_gb = "50"
      max_count = 1
      min_count = 0
      auto_upgrade = true
      auto_repair = true
      machine_type="n2-standard-2"
      node_locations = "europe-west1-b,europe-west1-c,europe-west1-d"
      image_type = "COS_CONTAINERD" 
      disable_legacy_metadata_endpoints = true
      node_pools_versions= "1.16.15-gke.4901"
    },
    {
      name = "pool-2"
      initial_node_count = "2"
      default_max_pods_per_node = "20" 
      disk_type = "pd-ssd" 
      disk_size_gb = "50"
      auto_upgrade = true
      auto_repair = true
      machine_type="n2-standard-2"
      image_type = "COS_CONTAINERD" 
      disable_legacy_metadata_endpoints = true
      node_pools_versions= "1.16.15-gke.4901"
    },
    {
      name = "pool-3"
      initial_node_count = "1"
      default_max_pods_per_node = "8" 
      disk_type = "pd-ssd" 
      disk_size_gb = "30"
      auto_upgrade = true
      auto_repair = true
      node_locations =  "europe-west1-b, europe-west1-d" 
      machine_type="n2-standard-2"
      image_type = "COS_CONTAINERD" 
      disable_legacy_metadata_endpoints = true
      node_pools_versions= "1.16.15-gke.4901"
    }	
  ]
 }
